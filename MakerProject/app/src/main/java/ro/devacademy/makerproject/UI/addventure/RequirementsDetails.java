package ro.devacademy.makerproject.UI.addventure;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import ro.devacademy.makerproject.Constants;
import ro.devacademy.makerproject.R;

public class RequirementsDetails extends AppCompatActivity {
    private static final String TAG = RequirementsDetails.class.getSimpleName();
    private TextView requirementDetails;

    private FrameLayout frameImage;
    private FrameLayout frameLinks;

    FragmentManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requirements_details);

        manager = getFragmentManager();

        Intent intent = getIntent();
        String asAText = intent.getStringExtra(Constants.AS_A_FIELD);
        String whenIWantText = intent.getStringExtra(Constants.WHEN_I_WANT_FIELD);
        String iShouldSeeText = intent.getStringExtra(Constants.I_SHOULD_SEE_FIELD);

        requirementDetails = (TextView) findViewById(R.id.requirementText);
        requirementDetails.setText("As a " + asAText + " when I want " + whenIWantText + " I should see " + iShouldSeeText);
//
//        Spannable spannable = (Spannable)requirementDetails.getText();
//
//        StyleSpan color = new StyleSpan(Color.BLUE);
//        spannable.setSpan(color, 0, 10, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);


        frameImage = (FrameLayout) findViewById(R.id.container_images);
        frameImage.setVisibility(View.GONE);

        frameLinks = (FrameLayout) findViewById(R.id.container_links);
        frameLinks.setVisibility(View.GONE);

        Log.i(TAG, "create: " + frameImage.isShown());
    }

    public void showImageFragment(View view) {

        if (frameImage.isShown() == true) {
            frameImage.setVisibility(View.GONE);
        }
        else {
            frameImage.setVisibility(View.VISIBLE);

            ImagesFragment fragment = new ImagesFragment();

            FragmentTransaction transaction = manager.beginTransaction();
            transaction.add(R.id.container_images, fragment, Constants.ADD_FRAGMENT_IMAGES);
            transaction.commit();
        }
    }

    public void showLinksFragment(View view) {
        if (frameLinks.isShown() == true){
            frameLinks.setVisibility(View.GONE);

        } else {
            frameLinks.setVisibility(View.VISIBLE);

            LinksFragment linksFragment = new LinksFragment();

            FragmentTransaction transaction = manager.beginTransaction();
            transaction.add(R.id.container_links, linksFragment, Constants.ADD_FRAGMENT_LINKS);
            transaction.commit();
        }
    }
}
