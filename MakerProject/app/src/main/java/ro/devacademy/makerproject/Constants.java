package ro.devacademy.makerproject;

/**
 * Created by Cata on 2/25/2017.
 */

public class Constants {
    public static final String AS_A_FIELD = "asAField";
    public static final String WHEN_I_WANT_FIELD = "whenIWantField";
    public static final String I_SHOULD_SEE_FIELD = "IShouldSeeField";

    public static final String ADD_FRAGMENT_IMAGES = "addFragmentImages";
    public static final String ADD_FRAGMENT_LINKS = "addFragmentLinks";
}
