package ro.devacademy.makerproject.server;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ro.devacademy.makerproject.models.Venture;
import ro.devacademy.makerproject.models.VenturesModel;
import ro.devacademy.makerproject.server.responses.VentureList;

/**
 * Created by Cata on 12/17/2016.
 */

public class ApiAdapter {
    private static ApiAdapter INSTANCE = null;
    private ServerApi serverApi = new ServerApi();

    private ApiAdapter(){

    }

    public static ApiAdapter getInstance(){
        if (INSTANCE == null)
            INSTANCE = new ApiAdapter();

        return INSTANCE;
    }

    public void getVentureData(final VenturesModel.VenturesCallback venturesCallback){
      venturesCallback.onVenturesReceived(serverApi.getVentures());
        //RETROFIT
    }
}
