package ro.devacademy.makerproject.UI.addventure;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.List;

import ro.devacademy.makerproject.R;

/**
 * Created by cata on 28.02.2017.
 */

public class ImageUploadAdapter extends RecyclerView.Adapter<ImageUploadAdapter.ImageHolder> {

    private LayoutInflater inflater;

    private List<Bitmap> imageViewList;

    public static class ImageHolder extends RecyclerView.ViewHolder {

        private ImageView image;

        public ImageHolder(View itemView) {
            super(itemView);

            this.image = (ImageView) itemView.findViewById(R.id.imageLoaded);
        }
    }


    public ImageUploadAdapter(List<Bitmap> imageViewList) {
        this.imageViewList = imageViewList;
    }

    @Override
    public ImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.from(parent.getContext()).inflate(R.layout.image_uploaded_format, parent, false);

        return new ImageHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ImageHolder holder, int position) {
        Bitmap bitmap = imageViewList.get(position);
        holder.image.setImageBitmap(bitmap);
    }

    @Override
    public int getItemCount() {
        return imageViewList.size();
    }

}
