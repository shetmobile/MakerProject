package ro.devacademy.makerproject.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ro.devacademy.makerproject.models.Venture;
import ro.devacademy.makerproject.server.responses.VentureList;

/**
 * Created by Cata on 12/17/2016.
 */

public class ServerApi {

    public List<Venture> getVentures(){

        List<Venture> listVenture = new ArrayList<>();
        listVenture.add(new Venture(0, "Create Alarm Clock", "", "blue", false, null));
        listVenture.add(new Venture(1, "My new app", "dam dam", "red", false, null));
        listVenture.add(new Venture(1, "crazy app", "party people", "green", false, null));

        return listVenture;
    }
}
