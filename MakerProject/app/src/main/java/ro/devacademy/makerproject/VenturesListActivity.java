package ro.devacademy.makerproject;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ro.devacademy.makerproject.UI.ventureslist.VentureProvider;
import ro.devacademy.makerproject.models.Venture;

public class VenturesListActivity extends AppCompatActivity {
    private List<Venture> ventureList = new ArrayList<>();
    private RecyclerView recyclerView;
    private VentureAdapter ventureAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventures_list);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        ventureAdapter = new VentureAdapter(ventureList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(ventureAdapter);
        recyclerView.addOnItemTouchListener(new VentureTouchListener(this, recyclerView, new VentureTouchListener.ClickListener(){

            @Override
            public void onClick(View view, int position) {
                Venture venture = ventureList.get(position);
                //TODO: start activity with position
                System.out.println("I'm here");
                Toast.makeText(VenturesListActivity.this, "" + venture.getName(), Toast.LENGTH_SHORT).show();
            }
        }));


        FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.new_venture_floating_action_button);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addVenture = new Intent(VenturesListActivity.this, AddNewVentureActivity.class);
                startActivity(addVenture);
            }
        });

        populate(ventureList);

    }

    public void populate(List<Venture> ventures){
        VentureProvider ventureProvider = VentureProvider.getInstance(VenturesListActivity.this);

        for(int i = 0; i<ventureProvider.getCount(); i++){
            ventures.add(ventureProvider.getItem(i));
        }

    }
}
