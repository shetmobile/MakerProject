package ro.devacademy.makerproject.models;

import java.util.List;

import ro.devacademy.makerproject.server.ApiAdapter;

/**
 * Created by Cata on 12/17/2016.
 */

public class VenturesModel {

    public void getVentures (VenturesCallback venturesCallback){
        //if data in DB -> return from DB
        //else -> network
        ApiAdapter.getInstance().getVentureData(venturesCallback);
    }

    public interface VenturesCallback {
        void onVenturesReceived(List<Venture> ventureList);
    }
}
