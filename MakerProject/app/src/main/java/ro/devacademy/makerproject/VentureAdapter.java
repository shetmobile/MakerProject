package ro.devacademy.makerproject;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ro.devacademy.makerproject.models.Venture;

/**
 * Created by radu on 17.12.2016.
 */

public class VentureAdapter extends RecyclerView.Adapter<VentureAdapter.MyViewHolder>{

    private List<Venture> ventureList;

    public VentureAdapter(List<Venture> ventureList) {
        this.ventureList = ventureList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.venture_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Venture venture = ventureList.get(position);

        holder.projectTitle.setText(venture.getName());
        if(venture.isHasIcon()){
            //TODO: icon
        }
        else{
            holder.projectIcon.setBackgroundColor(Color.parseColor(venture.getColor()));
        }
    }

    @Override
    public int getItemCount() {
        return ventureList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView projectTitle;
        public ImageView projectIcon;

        public MyViewHolder(View itemView) {
            super(itemView);
            projectTitle = (TextView) itemView.findViewById(R.id.project_title);
            projectIcon = (ImageView) itemView.findViewById(R.id.project_icon);
        }

        @Override
        public void onClick(View view) {

        }
    }

}
